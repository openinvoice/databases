# SPDX-FileCopyrightText: 2022 Hugo Rodrigues
#
# SPDX-License-Identifier: MIT

import argparse
from importlib import import_module
import json
import os
import pathlib
import re
import sys

import psycopg

# Constants
ROOT_PATH = pathlib.Path(__file__).parent.absolute()
INIT_SQL = os.path.join(ROOT_PATH, "init.sql")
MIGRATION_RE = re.compile(r"^[0-9]{10}$")

# Parse CLI

parser = argparse.ArgumentParser(description="OpenInvoice Database Migration")
parser.add_argument("component", type=str, help="OpenInvoice Component")
parser.add_argument("config", type=str, help="Component configuration file")
parser.add_argument("--demo", action="store_true", help="Run demo.sql scripts. Only available if database is initialized")

args = parser.parse_args()

# Parse configuration file
# We can use components configuration since they all share the same database configuration schema

assert os.path.isfile(args.config), "Configuration file not found or it isn't a file"

try:
    with open(args.config, "r") as cf:
        configuration = json.load(cf)
except json.decoder.JSONDecodeError:
    print("Invalid JSON configuration file", file=sys.stderr)
    sys.exit(1)

# Check if component has migrations

MIGRATIONS_PATH = os.path.join(ROOT_PATH, args.component)
assert os.path.isdir(MIGRATIONS_PATH), f"No migrations for {args.component} or {args.component} isn't a valid component"

# Build PostgreSQL connection string
## host and dbname are required
dbcn=f'host={configuration["database"]["host"]} dbname={configuration["database"]["dbname"]} application_name=openinvoice.migrations'

## build optional parameters
for attr in ("port", "user", "password", "sslmode", "sslcert", "sslkey", "sslpassword", "sslrootcert", "sslcrl", "sslcrldir", "sslsni"):
    if attr in configuration["database"]:
        dbcn += f' {attr}={configuration["database"][attr]}'

with psycopg.connect(dbcn) as cn:
    # Get current database version
    # If it fails, we will run the init.sql script
    with cn.cursor() as cr:
        try:
            with cn.transaction():
                # Get current database version and component
                # If system_parameter table doesn't exists, them the database isn't initialized
                cr.execute("""SELECT "value"::integer FROM "system_parameter" WHERE "key"='database.version'""")
                current_version = cr.fetchone()[0]
                cr.execute("""SELECT "value" FROM "system_parameter" WHERE "key"='database.component'""")
                db_component = cr.fetchone()[0]
        except psycopg.errors.UndefinedTable:
            # Database not initialized, will run
            #   * INIT_SQL (./init.sql),
            #   * set database.component
            #   * run ./$component/init.sql if exists
            current_version = 0
            db_component = args.component
            with cn.transaction():
                print(f"Initializing database for {args.component}")
                with open(INIT_SQL, "r") as init:
                    cr.execute(init.read())
                cr.execute("""INSERT INTO "system_parameter" ("key", "value") VALUES ('database.component', %s)""", (args.component,))

                if os.path.isfile(os.path.join(MIGRATIONS_PATH, "init.sql")):
                    with open(os.path.join(MIGRATIONS_PATH, "init.sql"), "r") as init:
                        cr.execute(init.read())

        # Ensure that the passed component maches the one inside the database
        assert db_component == args.component, f'Database belongs to {db_component} not {args.component}'

        # Find all migrations
        for migration in sorted(next(os.walk(MIGRATIONS_PATH))[1]):
            # Ensure that migration has a valid name
            # except for __pycache__ because Python
            if migration == "__pycache__":
                continue
            assert MIGRATION_RE.match(migration), f"{migration} isn't a valid migration name"

            # Only run migration if it's newer than the current version
            if int(migration) > current_version:
                print(f"Running migration {migration}")
                schema = os.path.join(MIGRATIONS_PATH, migration, "schema.sql")  # Direct SQL queries
                setup = os.path.join(MIGRATIONS_PATH, migration, "setup.py")     # PY script that contains two functions, one to run before and after schema.sql

                # Get pre and post schema functions, if set
                setup_pre = setup_post = None
                if os.path.exists(setup):
                    module = import_module(f"{args.component}.{migration}.setup")
                    setup_pre = getattr(module, "pre_schema", None)
                    setup_post = getattr(module, "post_schema", None)

                with cn.transaction():

                    # Run pre function
                    if callable(setup_pre):
                        setup_pre(cr)

                    # Run SQL script
                    if os.path.exists(schema):
                        with open(schema, "r") as sql:
                            cr.execute(sql.read())

                    # Run post function
                    if callable(setup_post):
                        setup_post(cr)

                    # Set current version
                    cr.execute("""UPDATE "system_parameter" SET "value"=%s WHERE "key"='database.version'""", (migration,))

        # Run demo schema
        if os.path.isfile(os.path.join(MIGRATIONS_PATH, "demo.sql")) and current_version == 0 and args.demo:
            print("Running demo.sql")
            with open(os.path.join(MIGRATIONS_PATH, "demo.sql"), "r") as sql:
                with cn.transaction():
                    cr.execute(sql.read())
