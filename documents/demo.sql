-- SPDX-FileCopyrightText: 2022 Hugo Rodrigues
--
-- SPDX-License-Identifier: MIT

INSERT INTO "realm" ("identifier", "company_name", "email", "fiscal_number", "address", "country", "postal_code") VALUES ('83f54d03-31bd-11ed-a60e-b445061c211b', 'Demo', 'demo@demo.com', '509001002', 'Main Street', 'PT', '1900-001');
INSERT INTO "sequence" ("sequence", "realm_id", "document_type_id") VALUES ('2022', 1, 1);
