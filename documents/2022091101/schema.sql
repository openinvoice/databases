-- SPDX-FileCopyrightText: 2022 Hugo Rodrigues
--
-- SPDX-License-Identifier: MIT

-- Initial migration for documents

-- Company details
ALTER TABLE "realm" ADD COLUMN "company_name" varchar NOT NULL;
ALTER TABLE "realm" ADD COLUMN "business_name" varchar;
ALTER TABLE "realm" ADD COLUMN "email" varchar NOT NULL;
ALTER TABLE "realm" ADD COLUMN "fiscal_number" varchar NOT NULL;
ALTER TABLE "realm" ADD COLUMN "address" varchar NOT NULL;
ALTER TABLE "realm" ADD COLUMN "country" char(2) NOT NULL;
ALTER TABLE "realm" ADD COLUMN "postal_code" varchar NOT NULL;
ALTER TABLE "realm" ADD COLUMN "phone" varchar;
ALTER TABLE "realm" ADD COLUMN "fax" varchar;
ALTER TABLE "realm" ADD COLUMN "website" varchar;

-- Taxes
CREATE TABLE "taxes" (
    "id" serial,
    "name" varchar(10) NOT NULL,
    "value" numeric(6, 3) NOT NULL,
    "region" varchar(5) NOT NULL,
    "code" varchar(10) NOT NULL,
    "type" varchar(3) NOT NULL,
    "description" varchar(255) NOT NULL,
    "valid_until" date, -- When set, don't allow the usage of the tax after the date
    PRIMARY KEY ("id"),
    CONSTRAINT "taxes_unique_name" UNIQUE ("name"),
    CONSTRAINT "taxes_check_type" CHECK ("type" IN ('IVA', 'IS', 'NS'))
);

CREATE INDEX "taxes_idx_name" ON "taxes" USING HASH ("name");

-- Portugal - Mainland
INSERT INTO "taxes" ("name", "value", "region", "code", "type", "description") VALUES ('IVA6', 6.0, 'PT', 'RED', 'IVA', 'IVA6');
INSERT INTO "taxes" ("name", "value", "region", "code", "type", "description") VALUES ('IVA13', 13.0, 'PT', 'INT', 'IVA', 'IVA13');
INSERT INTO "taxes" ("name", "value", "region", "code", "type", "description") VALUES ('IVA23', 23.0, 'PT', 'NOR', 'IVA', 'IVA23');

-- Portugal - Madeira Region
INSERT INTO "taxes" ("name", "value", "region", "code", "type", "description") VALUES ('IVA5', 5.0, 'PT-MA', 'RED', 'IVA', 'IVA5');
INSERT INTO "taxes" ("name", "value", "region", "code", "type", "description") VALUES ('IVA12', 12.0, 'PT-MA', 'INT', 'IVA', 'IVA12');
INSERT INTO "taxes" ("name", "value", "region", "code", "type", "description") VALUES ('IVA22', 22.0, 'PT-MA', 'NOR', 'IVA', 'IVA22');

-- Portugal - Azores Region
INSERT INTO "taxes" ("name", "value", "region", "code", "type", "description") VALUES ('IVA4', 4.0, 'PT-AC', 'RED', 'IVA', 'IVA4');
INSERT INTO "taxes" ("name", "value", "region", "code", "type", "description") VALUES ('IVA9', 9.0, 'PT-AC', 'INT', 'IVA', 'IVA9');
INSERT INTO "taxes" ("name", "value", "region", "code", "type", "description") VALUES ('IVA16', 16.0, 'PT-AC', 'NOR', 'IVA', 'IVA16');

-- Portugal - All
INSERT INTO "taxes" ("name", "value", "region", "code", "type", "description") VALUES ('IVA0', 0, 'PT', 'IS', 'IVA', 'IVA0');

CREATE TRIGGER taxes_trg_prevent_update
    BEFORE UPDATE OF "value", "region", "code", "type"
    ON "taxes"
    FOR EACH ROW
    EXECUTE PROCEDURE fn_prevent_update();

-- Document types
CREATE TABLE "document_type" (
    "id" serial,
    "name" varchar NOT NULL,
    "code" char(2) NOT NULL,
    "saft_section" varchar(20) NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "document_type_unique_code" UNIQUE ("code", "saft_section")
);

CREATE INDEX "document_type_idx_code" ON "document_type" USING HASH("code");

-- saft_section will be set later.
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Invoice', 'FT', 'salesinvoice');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Simplified Invoice', 'FS', 'salesinvoice');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Invoice Receipt', 'FR', 'salesinvoice');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Debit Note', 'ND', 'salesinvoice');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Credit Note', 'NC', 'salesinvoice');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Shipping Waybill', 'GR', 'movementofgoods');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Transport Waybill', 'GT', 'movementofgoods');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Own Asset Waybill', 'GA', 'movementofgoods');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Consignment Waybill', 'GC', 'movementofgoods');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Devolution Waybill', 'GD', 'movementofgoods');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Table Consultation', 'CM', 'workingdocuments');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Consignation Credit', 'CC', 'workingdocuments');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Consignation Invoice', 'FC', 'workingdocuments');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Work Sheet', 'FO', 'workingdocuments');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Order', 'NE', 'workingdocuments');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Other Work Document', 'OU', 'workingdocuments');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Quote', 'OR', 'workingdocuments');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Pro-Forma Invoice', 'PF', 'workingdocuments');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Payment', 'RC', 'payment');
INSERT INTO "document_type" ("name", "code", "saft_section") VALUES ('Other Payment', 'RG', 'payment');

-- sequences
CREATE TABLE "sequence" (
    "id" serial,
    "sequence" varchar(255) NOT NULL,
    "current_number" integer NOT NULL DEFAULT 0,
    "document_type_id" integer NOT NULL references document_type(id),
    "realm_id" integer NOT NULL references realm(id),
    PRIMARY KEY ("id")
);

CREATE INDEX "sequence_idx_sequence" ON "sequence" ("sequence", "document_type_id", "realm_id");

CREATE TRIGGER sequence_trg_prevent_update_
    BEFORE UPDATE OF "sequence", "document_type_id", "realm_id"
    ON "sequence"
    FOR EACH ROW
    EXECUTE PROCEDURE fn_prevent_update();

-- Address
-- This is a one to one, but keeps other tables clean
CREATE TABLE "address" (
    "id" serial,
    "building_number" varchar(10),
    "street" varchar(200),
    "address" varchar(210) NOT NULL,
    "city" varchar(50) NOT NULL,
    "postalcode" varchar(20) NOT NULL,
    "region" varchar(50),
    "country" varchar(12) NOT NULL DEFAULT 'Desconhecido',
    PRIMARY KEY ("id")
);

CREATE TABLE "delivery" (
    "id" serial,
    "address_id" integer NOT NULL references address(id),
    "date" timestamp with time zone NOT NULL,
    "warehouse" varchar(50),
    "location" varchar(20),
    PRIMARY KEY ("id")
);

CREATE TABLE "delivery_identification" (
    "id" serial,
    "delivery_id" integer NOT NULL references delivery(id),
    "identification" varchar(255) NOT NULL,
    PRIMARY KEY ("id")
);

-- Document status
CREATE TABLE "document_status" (
    "id" serial,
    "name" varchar(10) NOT NULL,
    "description" varchar NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "document_status_unique_name" UNIQUE ("name")
);

INSERT INTO "document_status" ("name", "description") VALUES ('draft', 'Draft');
INSERT INTO "document_status" ("name", "description") VALUES ('final', 'Final');
INSERT INTO "document_status" ("name", "description") VALUES ('settled', 'Settled');
INSERT INTO "document_status" ("name", "description") VALUES ('unsettled', 'Unsettled');
INSERT INTO "document_status" ("name", "description") VALUES ('canceled', 'Canceled');
INSERT INTO "document_status" ("name", "description") VALUES ('refused', 'Refused');
INSERT INTO "document_status" ("name", "description") VALUES ('accepted', 'Accepted');

-- Documents

CREATE TABLE "document" (
    "id" serial,
    "sequence_id" integer NOT NULL references sequence(id),
    "hash" varchar(172),
    "date" timestamp with time zone NOT NULL,
    "selfbilling" boolean NOT NULL default false,
    "cashvat" boolean NOT NULL default false,
    "thirdpartybilling" boolean NOT NULL default false,
    "responsible" varchar(30) NOT NULL,
    "load_delivery_id" integer references delivery(id),
    "unload_delivery_id" integer references delivery(id),
    "status_id" integer NOT NULL references document_status(id),
    "status_date" timestamp with time zone NOT NULL DEFAULT (now() at timezone 'utc')
    "status_responsible" varchar(30) NOT NULL,

    PRIMARY KEY ("id")
);

-- Document EAC Code
CREATE TABLE "document_eaccode" (
    "id" serial,
    "document_id" integer NOT NULL references document(id),
    "code" char(5) NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "document_eaccode_check_code" CHECK ("code" ~ '^[0-9]{5}$')
);


-- Clients on documents
-- This is a one to one relation, because each documents must have a copy of the client details

CREATE TABLE "document_client" (
    "id" serial,
    "name" varchar NOT NULL,
    "email" varchar,
    "postal_code" varchar NOT NULL,
    "country" char(2) NOT NULL,
    "fiscal_number" varchar NOT NULL,
    "website" varchar,
    "phone" varchar,
    "fax" varchar,
    "observations" varchar,
    "document_id" integer NOT NULL references document(id),
    PRIMARY KEY ("id")
);

-- Master client data
-- It holds the reference for the latest client information on document_client
CREATE TABLE "client" (
    "realm_id" integer NOT NULL references realm(id),
    "document_client_id" integer NOT NULL references document_client(id),
    "fiscal_number" varchar NOT NULL,
    PRIMARY KEY ("realm_id", "document_client_id", "fiscal_number"),
);

-- Document lines
CREATE TABLE "document_line" (
    "id" serial,
    "description" text NOT NULL,
    "unit_price" numeric(12, 4),

