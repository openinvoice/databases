-- SPDX-FileCopyrightText: 2022 Hugo Rodrigues
--
-- SPDX-License-Identifier: MIT

-- Database init
-- All databases have this tables since they are required for migrations

CREATE TABLE "system_parameter" (
    "id" serial,
    "key" varchar NOT NULL,
    "value" TEXT NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "system_parameter_unique_key" UNIQUE ("key")
);

CREATE INDEX "system_parameter_idx_key" ON "system_parameter" USING HASH ("key");

CREATE TABLE "realm" (
    "id" serial,
    "identifier" UUID NOT NULL,
    PRIMARY KEY ("id"),
    CONSTRAINT "realm_unique_identifier" UNIQUE ("identifier")
);

CREATE INDEX "reaml_idx_identifier" ON "realm" USING HASH ("identifier");
INSERT INTO "system_parameter" ("key", "value") VALUES ('database.version', '0');

CREATE FUNCTION fn_prevent_update()
    RETURNS trigger AS
$BODY$
    BEGIN
        RAISE EXCEPTION 'Some columns can not be updated';
    END;
$BODY$
    LANGUAGE plpgsql VOLATILE
    COST 100;
