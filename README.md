<!--
SPDX-FileCopyrightText: 2022 Hugo Rodrigues

SPDX-License-Identifier: CC0-1.0
-->

# OpenInvoice Databases

Database migration tool for all OpenInvoice databases

## Why here and not on each component repository

Each module could manage his own database migrations, but then we will have code duplication.

## How to create a migration


For each component, lets say *documents*, we need to create a Python module.

Inside the component directory (aka Python module), we have:
* Each migration is represented by a folder with the format *YYYYMMDDXX* where *XX* represents a two digit index;
* \_\_init\_\_.py - Mark this directory as a Python module. Required for Python migrations;
* demo.sql - SQL to be executed if demo mode is enabled;
* init.sql - SQL to be executed when the database is initialized.

Inside each migration, we have two files:

* schema.sql - This is a bare SQL script;
* setup.py - Python file that can have two functions:
    * pre_schema - Will be executed before schema.sql
    * post_schema - Will be executed after schema.sql

**pre_schema** and **post_schema** receive the current database cursor as a positional argument.

### Folder structure relative to this repository root

```
documents/
├── 2022091001
│   ├── schema.sql
│   └── setup.py
├── __init__.py
└── demo.sql
└── init.sql
```

## migrate.py

This is the script that will run the migration. It requires two, positional, arguments:
1. Component to be migrated
2. Configuration file

### Usage
```python3 migrate.py component config```

### Configuration file
Since all components share the same structure for database connections, it's possible to use the component configuration directly.

